package com.rileymills.mpermissionstest;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import android.content.pm.PackageManager;
import android.Manifest;
import android.widget.Toast;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap;
    private boolean canAccessLocation;
    public static final int LOCATION_PERMISSION_CALLBACK_ID = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        canAccessLocation = false;
        setContentView(R.layout.activity_maps);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUpMap();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
            String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_CALLBACK_ID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Got Permission", Toast.LENGTH_LONG).show();
                    canAccessLocation = true;
                } else {
                    Toast.makeText(getApplicationContext(), "Denied Permission", Toast.LENGTH_LONG).show();
                    canAccessLocation = false;
                }
                return;
            }

            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }

    private void setUpMap() {
        ensurePermissions();

        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

            if (mMap != null) {
                addMapMarkers();
            }
        }

        if(canAccessLocation){
            mMap.setMyLocationEnabled(true);
        }
    }

    private void addMapMarkers() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    private void ensurePermissions(){
        if (checkSelfPermission(Manifest.permission_group.LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CALLBACK_ID);
        }else{
            Toast.makeText(getApplicationContext(), "Still Have Permission", Toast.LENGTH_LONG).show();
            canAccessLocation = true;
        }
    }


}
